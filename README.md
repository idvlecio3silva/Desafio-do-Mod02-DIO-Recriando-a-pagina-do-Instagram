# Sejam todos bem-vindos ao meu repositório 👨‍💻

Nesse repositório apresento o resultado dos conhecimentos obtidos nesse segundo módulo desse bootcamp. Esse resultado consiste em uma recriação da página do Instagram. Para isso, foi usado essencialmente HTML, CSS e Flexbox. 


Veja como ficou na imagem e dê um olhada no meu código😎👍 

<div align-"center">
<img src="https://user-images.githubusercontent.com/66226187/180329732-a4c40ab7-b4bf-481f-a091-fe987ee4fa48.png"
</div>
